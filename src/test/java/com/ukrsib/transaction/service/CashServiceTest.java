package com.ukrsib.transaction.service;

import com.ukrsib.transaction.model.HashTransactions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CashServiceTest {

    @Autowired
    private CashService cashService;

    @Test
    public void cashTransaction() {
        HashTransactions hashTransactions = cashService.getHashTransactions();
        assertNotNull(hashTransactions);
    }

    @Test
    public void cashTransactionSize(){
        HashTransactions hashTransactions = cashService.getHashTransactions();
        assertTrue(hashTransactions.getTransactions().keySet().size() > 0);
    }

    @Test
    public void cashSomeRequest() {
        ExecutorService executor = Executors.newFixedThreadPool(5);
        List<Callable<HashTransactions>> callables = Arrays.asList(
                () -> cashService.getHashTransactions(),
                () -> cashService.getHashTransactions(),
                () -> cashService.getHashTransactions(),
                () -> cashService.getHashTransactions(),
                () -> cashService.getHashTransactions(),
                () -> cashService.getHashTransactions()
        );
        try {
            executor.invokeAll(callables)
                    .stream()
                    .map(future -> {
                        try {
                            return future.get().getTransactions().entrySet().size();
                        } catch (Exception e) {
                            throw new IllegalStateException(e);
                        }
                    }).forEach(size -> assertTrue(size > 0));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void makeCloneHashTransactions(){
       HashTransactions transactions = cashService.getHashTransactions();
       HashTransactions anotherTransactions = cashService.getHashTransactions();
       transactions.getTransactions().put("testClone",new ArrayList<>());
       assertFalse(anotherTransactions.getTransactions().containsKey("testClone"));
    }
}
