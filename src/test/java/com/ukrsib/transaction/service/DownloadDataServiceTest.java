package com.ukrsib.transaction.service;

import com.ukrsib.transaction.enums.DocumentNameTagEnumeration;
import com.ukrsib.transaction.enums.FilePathEnumeration;
import com.ukrsib.transaction.exceptions.IllegalArgumentTransactionExceptions;
import com.ukrsib.transaction.model.Transactions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DownloadDataServiceTest {

    @Autowired
    private DownloadDataService downloadDataService;
    @Autowired
    private MessageService messageService;
    private final String testDataTransactional = "<transactions>" +
            "<transaction> " +
            "<place>A PLACE 1</place>" +
            "<amount>10.01</amount>" +
            "<currency>UAH</currency>" +
            "<card>123456****1234</card>" +
            "<client>" +
            "<firstName>Ivan</firstName>\n" +
            "<lastName>Ivanoff</lastName>" +
            "<middleName>Ivanoff</middleName>" +
            "<inn>1234567890</inn>" +
            "</client>" +
            "</transaction>" +
            "<transaction>" +
            "<place>A PLACE 2</place>" +
            "<amount>9876.01</amount>" +
            "<currency>UAH</currency>" +
            "<card>123456****1234</card>" +
            "<client>" +
            "<firstName>Ivan</firstName>" +
            "<lastName>Ivanoff</lastName>" +
            "<middleName>Ivanoff</middleName>" +
            "<inn>1234567890</inn>" +
            "</client>" +
            "</transaction>" +
            "</transactions>";
    @Test
    public void getInputStream() {
        try {
            FileInputStream fileInputStream = downloadDataService.getFileInputStream(messageService.getMessage(
                    FilePathEnumeration.PATH_TEST_XML.getPath()));
            assertNotNull(fileInputStream);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    @Test(expected = IllegalArgumentTransactionExceptions.class)
    public void notCorrectPathXmlFile() {
        String notCorrectPath = "/test/mistake.xml";
        try {
            FileInputStream fileInputStream = downloadDataService.getFileInputStream(notCorrectPath);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Test(expected = IllegalArgumentTransactionExceptions.class)
    public void PathXmlFileIsNull() {
        try {
            FileInputStream fileInputStream = downloadDataService.getFileInputStream(null);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void getAllTransactions() {
        Transactions transactions = downloadDataService.getAllTransactions();
        assertNotNull(transactions);
    }

    @Test
    public void getAllTransactionsSize() throws Exception {
        Transactions transactions = downloadDataService.getAllTransactions();
        assertTrue(transactions.getTransaction().size() > 0);
    }

    @Test
    public void unmarshalling() {

        try {
            InputStream stream = new ByteArrayInputStream(testDataTransactional.getBytes(StandardCharsets.UTF_8));
            XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
            XMLStreamReader xmlStream = xmlInputFactory.createXMLStreamReader(stream);
            downloadDataService.setStartElement(xmlStream,DocumentNameTagEnumeration.TRANSACTIONS.getNameTag());
            Transactions transactions = downloadDataService.unmarshallingTransactions(xmlStream);
            assertNotNull(transactions);
            assertEquals(transactions.getTransaction().size(), 2);
        } catch (javax.xml.stream.XMLStreamException | JAXBException e) {
            e.printStackTrace();
        }

    }
}
