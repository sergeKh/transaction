package com.ukrsib.transaction.pages.transuction;

import com.ukrsib.transaction.enums.TransactionClientLabelId;
import com.ukrsib.transaction.enums.TransactionLabelId;
import com.ukrsib.transaction.model.Transaction;
import com.ukrsib.transaction.pages.BasePage;
import com.ukrsib.transaction.service.TransactionService;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.List;
import java.util.Optional;

public class TransactionsModalView extends BasePage {
    @SpringBean
    private TransactionService transactionService;

    public TransactionsModalView(String inn, String firstName, String lastName) {
        Optional<ListView> transactionsClientListView = getTransactionsClientListView(inn);
        if (transactionsClientListView.isPresent()) {
            this.add(transactionsClientListView.get());
        }
        this.add(new Label(TransactionClientLabelId.FIRST_NAME.getId(), firstName));
        this.add(new Label(TransactionClientLabelId.LAST_NAME.getId(), lastName));
    }

    public Optional<ListView> getTransactionsClientListView(String inn) {
        List<Transaction> listTransactions = transactionService.getListTransactionByInn(inn);
        if (listTransactions == null) {
            return Optional.empty();
        }
        ListView movieListView = new ListView("transaction_list", listTransactions) {
            @Override
            protected void populateItem(ListItem item) {
                Transaction transaction = (Transaction) item.getModelObject();
                item.add(new Label(TransactionLabelId.PLACE.getId(), transaction.getPlace()));
                item.add(new Label(TransactionLabelId.CURRENCY.getId(), transaction.getCurrency()));
                item.add(new Label(TransactionLabelId.AMOUNT.getId(), transaction.getAmount()));
                item.add(new Label(TransactionLabelId.CARD.getId(), transaction.getCard()));
            }
        };
        return Optional.ofNullable(movieListView);
    }
}
