package com.ukrsib.transaction.pages.transuction;

import com.giffing.wicket.spring.boot.context.scan.WicketHomePage;
import com.ukrsib.transaction.enums.TransactionClientLabelId;
import com.ukrsib.transaction.model.Client;
import com.ukrsib.transaction.pages.BasePage;
import com.ukrsib.transaction.service.TransactionService;
import org.apache.wicket.Page;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.annotation.mount.MountPath;

import java.util.List;
import java.util.Optional;

@WicketHomePage
@MountPath("transactions")
public class TransactionsPage extends BasePage {
    @SpringBean
    private TransactionService transactionService;

    private ModalWindow modalWindow;

    public TransactionsPage() {
        Optional<ListView> clientListView = getClientListView();
        if (clientListView.isPresent()) {
            this.add(clientListView.get());
        }
        this.modalWindow = createModalWindow();
        this.add(modalWindow);
    }

    public Optional<ListView> getClientListView() {
        List<Client> listClient = transactionService.getListClient();
        if (listClient == null) {
            return Optional.empty();
        }
        ListView movieListView = new ListView("client_list", listClient) {
            @Override
            protected void populateItem(ListItem item) {
                Client client = (Client) item.getModelObject();
                item.add(new Label(TransactionClientLabelId.INN.getId(), client.getInn()));
                item.add(new Label(TransactionClientLabelId.FIRST_NAME.getId(), client.getFirstName()));
                item.add(new Label(TransactionClientLabelId.MIDDLE_NAME.getId(), client.getMiddleName()));
                item.add(new Label(TransactionClientLabelId.LAST_NAME.getId(), client.getLastName()));
                item.add(getAjaxLinkTransactions(client.getInn(), client.getFirstName(), client.getLastName()));
            }
        };
        return Optional.ofNullable(movieListView);
    }

    public AjaxLink getAjaxLinkTransactions(String inn, String firstName, String lastName) {
        AjaxLink ajaxLink = new AjaxLink<Object>("view-transactions") {
            @Override
            public void onClick(AjaxRequestTarget target) {
                updateTransactionsModalWindow(inn, firstName, lastName);
                modalWindow.show(target);
            }
        };
        return ajaxLink;
    }


    public ModalWindow createModalWindow() {
        ModalWindow modalWindow = new ModalWindow("modal-transactions");
        modalWindow.setTitle("$");
        return modalWindow;
    }

    public void updateTransactionsModalWindow(String inn, String firstName, String lastName) {
        modalWindow.setPageCreator(new ModalWindow.PageCreator() {
            @Override
            public Page createPage() {
                return new TransactionsModalView(inn, firstName, lastName);
            }
        });
    }

}
