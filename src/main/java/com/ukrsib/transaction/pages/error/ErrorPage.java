package com.ukrsib.transaction.pages.error;

import com.ukrsib.transaction.enums.NameErrorFieldEnumerations;
import com.ukrsib.transaction.pages.BasePage;
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.wicketstuff.annotation.mount.MountPath;

@MountPath("error")
public class ErrorPage extends BasePage {
    private PageParameters params;

    public ErrorPage(PageParameters params) {
        super(params);
        initPage(params);
    }

    private void initPage(PageParameters params) {
        String errorMessage = StringUtils.EMPTY;
        if (params != null
                && params.get(NameErrorFieldEnumerations.ERROR_FIELD_MESSAGES.getNameField()) != null) {
            errorMessage = params.get(NameErrorFieldEnumerations.ERROR_FIELD_MESSAGES.getNameField()).toString();
        }
        this.add(new Label(NameErrorFieldEnumerations.ERROR_FIELD_MESSAGES.getNameField(), errorMessage));
    }
}
