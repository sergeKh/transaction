package com.ukrsib.transaction.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HashTransactions implements Serializable, Cloneable {
    private Map<String, List<Transaction>> transactions;
    private List<Client> clients;
    private Long fileSize;

    public HashTransactions() {
        super();
    }

    public HashTransactions(Map<String, List<Transaction>> transactions, List<Client> clients) {
        this.transactions = transactions;
        this.clients = clients;
    }

    public Map<String, List<Transaction>> getTransactions() {
        return transactions;
    }

    public void setTransactions(Map<String, List<Transaction>> transactions) {
        this.transactions = transactions;
    }

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }


    @Override
    public HashTransactions clone() throws CloneNotSupportedException {
        HashTransactions copy = (HashTransactions) super.clone();
        copy.setTransactions(new HashMap<String, List<Transaction>>(transactions));
        copy.setClients(new ArrayList<>(clients));
        return copy;
    }
}
