package com.ukrsib.transaction.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@XmlRootElement(name = "transaction")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Transaction implements Serializable, Cloneable {
    private String place;
    private BigDecimal amount;
    private String currency;
    private String card;
    private Client client;

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transaction)) return false;
        Transaction that = (Transaction) o;
        return Objects.equals(getPlace(), that.getPlace()) &&
                Objects.equals(getAmount(), that.getAmount()) &&
                Objects.equals(getCurrency(), that.getCurrency()) &&
                Objects.equals(getCard(), that.getCard()) &&
                Objects.equals(getClient(), that.getClient());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getPlace(), getAmount(), getCurrency(), getCard(), getClient());
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "place='" + place + '\'' +
                ", amount=" + amount +
                ", currency='" + currency + '\'' +
                ", card='" + card + '\'' +
                ", client=" + client +
                '}';
    }

    @Override
    protected Transaction clone() throws CloneNotSupportedException {
        Transaction transaction = (Transaction) super.clone();
        transaction.setClient(this.client.clone());
        return transaction;
    }
}

