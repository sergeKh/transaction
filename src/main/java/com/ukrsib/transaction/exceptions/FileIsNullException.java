package com.ukrsib.transaction.exceptions;

import com.ukrsib.transaction.pages.error.ErrorPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class FileIsNullException extends TransactionDataException {
    public FileIsNullException(Class<ErrorPage> pageClass, PageParameters params) {
        super(pageClass, params);
    }

    public FileIsNullException(String message) {
        super(message);
    }
}
