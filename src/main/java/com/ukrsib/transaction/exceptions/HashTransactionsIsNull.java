package com.ukrsib.transaction.exceptions;

import com.ukrsib.transaction.pages.error.ErrorPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class HashTransactionsIsNull extends  TransactionDataException {
    public HashTransactionsIsNull(Class<ErrorPage> pageClass, PageParameters params) {
        super(pageClass, params);
    }

    public HashTransactionsIsNull(String message) {
        super(message);
    }
}
