package com.ukrsib.transaction.exceptions;

import com.ukrsib.transaction.pages.error.ErrorPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class NotCorrectFileFormatException extends TransactionDataException {

    public NotCorrectFileFormatException(Class<ErrorPage> pageClass, PageParameters params) {
        super(pageClass, params);
    }

    public NotCorrectFileFormatException(String message) {
        super(message);
    }
}
