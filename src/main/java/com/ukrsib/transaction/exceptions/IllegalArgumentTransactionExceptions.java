package com.ukrsib.transaction.exceptions;

import com.ukrsib.transaction.pages.error.ErrorPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class IllegalArgumentTransactionExceptions extends TransactionDataException {
    public IllegalArgumentTransactionExceptions(Class<ErrorPage> pageClass, PageParameters params) {
        super(pageClass, params);
    }

    public IllegalArgumentTransactionExceptions(String message) {
        super(message);
    }
}
