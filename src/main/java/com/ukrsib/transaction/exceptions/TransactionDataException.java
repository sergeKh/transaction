package com.ukrsib.transaction.exceptions;

import com.ukrsib.transaction.enums.NameErrorFieldEnumerations;
import com.ukrsib.transaction.pages.error.ErrorPage;
import org.apache.wicket.RestartResponseException;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class TransactionDataException extends RestartResponseException {

    public TransactionDataException(Class<ErrorPage> pageClass, PageParameters params) {
        super(pageClass, params);
    }

    public TransactionDataException(String message) {

        super(ErrorPage.class, new PageParameters()
                .add(
                        NameErrorFieldEnumerations.ERROR_FIELD_MESSAGES.getNameField(),
                        message));
    }
}
