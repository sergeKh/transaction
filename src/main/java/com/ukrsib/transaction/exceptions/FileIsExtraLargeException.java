package com.ukrsib.transaction.exceptions;

import com.ukrsib.transaction.pages.error.ErrorPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class FileIsExtraLargeException extends TransactionDataException {
    public FileIsExtraLargeException(Class<ErrorPage> pageClass, PageParameters params) {
        super(pageClass, params);
    }

    public FileIsExtraLargeException(String message) {
        super(message);
    }
}
