package com.ukrsib.transaction.service;

import com.ukrsib.transaction.enums.NameErrorMessageEnumeration;
import com.ukrsib.transaction.exceptions.HashTransactionsIsNull;
import com.ukrsib.transaction.model.Client;
import com.ukrsib.transaction.model.HashTransactions;
import com.ukrsib.transaction.model.Transaction;
import com.ukrsib.transaction.model.Transactions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.lang.ref.SoftReference;
import java.util.*;


@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class CashService {
    @Autowired
    private MessageService messageService;

    @Autowired
    private DownloadDataService downloadDataService;
    private final static Integer NUMBER_TRANSACTIONS_ONE_CLIENT = 4;
    private SoftReference<HashTransactions> hashTransactions;


    public synchronized HashTransactions getHashTransactions() {
        Long fileSize = downloadDataService.getSizeFileXml();
        if (hashTransactions != null && fileSize.equals(hashTransactions.get().getFileSize())) {
            return makeCloneHashTransactions();
        }
        Optional<HashTransactions> hashT = transactions();
        if (!hashT.isPresent()) {
            throw new HashTransactionsIsNull(messageService.getMessage(
                    NameErrorMessageEnumeration.NOT_CORRECT_FORMAT_FILE.getNameMessage()));//todo add correct exception
        }
        hashTransactions = new SoftReference<HashTransactions>(hashT.get());
        hashTransactions.get().setFileSize(fileSize);
        return makeCloneHashTransactions();
    }

    private Optional<HashTransactions> transactions() {
        Transactions transactions = downloadDataService.getAllTransactions();
        if (transactions == null || transactions.getTransaction() == null) {
            return Optional.empty();
        }
        Integer size = transactions.getTransaction().size();
        size = size / NUMBER_TRANSACTIONS_ONE_CLIENT;
        List<Client> listClient = new LinkedList<Client>();
        Map<String, List<Transaction>> transactionMap = new HashMap<>(size);
        String innClient = "";
        for (Transaction transaction : transactions.getTransaction()) {
            if (transaction.getClient() == null) {
                continue;//todo log
            }
            innClient = transaction.getClient().getInn();
            if (checkContainsClient(transactionMap, innClient)) {
                transactionMap.get(innClient).add(transaction);
            } else {
                addTransaction(transactionMap, transaction, innClient);
                listClient.add(transaction.getClient());
            }
        }
        return Optional.of(new HashTransactions(transactionMap, listClient));
    }

    private boolean checkContainsClient(Map<String, List<Transaction>> transactionMap, String key) {
        if (transactionMap == null || key == null) {
            return false;
        }
        return transactionMap.containsKey(key);
    }

    private void addTransaction(Map<String, List<Transaction>> transactionMap,
                                Transaction transaction,
                                String key) {
        if (transactionMap == null || key == null || transaction == null) {
            return;
        }
        List<Transaction> clientTransactions = new LinkedList<Transaction>();
        clientTransactions.add(transaction);
        transactionMap.put(key, clientTransactions);
    }

    private HashTransactions makeCloneHashTransactions() {
        if (hashTransactions == null || hashTransactions.get() == null) {
            new HashTransactions();
        }
        try {
            return hashTransactions.get().clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return new HashTransactions();
    }

}
