package com.ukrsib.transaction.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MessageService {
    @Autowired
    private MessageSource messageSource;

    public String getMessage(String nameMessage) {
        Optional.ofNullable(nameMessage)
                .orElseThrow(IllegalArgumentException::new);
        return messageSource.getMessage(nameMessage, null, null);
    }
}
