package com.ukrsib.transaction.service;

import com.ukrsib.transaction.model.Client;
import com.ukrsib.transaction.model.HashTransactions;
import com.ukrsib.transaction.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class TransactionService {
    @Autowired
    private CashService cashService;

    public List<Client> getListClient() {
        HashTransactions transactions = cashService.getHashTransactions();
        if (transactions == null) {
            return Collections.emptyList();
        }
        return transactions.getClients();
    }

    public List<Transaction> getListTransactionByInn(final String inn) {
        if (inn == null || inn.isEmpty()) {
            return Collections.emptyList();
        }
        HashTransactions transactions = cashService.getHashTransactions();
        if (transactions == null) {
            return Collections.emptyList();
        }
        Map<String, List<Transaction>> transactionMap = transactions.getTransactions();
        if (transactionMap == null || transactionMap.isEmpty()) {
            return Collections.emptyList();
        }
        return transactionMap.get(inn);
    }
}
