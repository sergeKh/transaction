package com.ukrsib.transaction.service;

import com.ukrsib.transaction.enums.DocumentNameTagEnumeration;
import com.ukrsib.transaction.enums.FilePathEnumeration;
import com.ukrsib.transaction.enums.NameErrorMessageEnumeration;
import com.ukrsib.transaction.exceptions.FileIsNullException;
import com.ukrsib.transaction.exceptions.IllegalArgumentTransactionExceptions;
import com.ukrsib.transaction.exceptions.NotCorrectFileFormatException;
import com.ukrsib.transaction.model.Transactions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;

@Service
public class DownloadDataService {
    @Autowired
    private MessageService messageService;

    private final static String CLASS_LOADER_IS_NULL = "Class Loader is null.";

    public FileInputStream getFileInputStream(String path) throws IOException {
        Optional.ofNullable(path)
                .orElseThrow(() -> new IllegalArgumentTransactionExceptions(
                        messageService.getMessage(
                                NameErrorMessageEnumeration.PATH_IS_NULL.getNameMessage())));
        ClassLoader classLoader = getClass().getClassLoader();
        assert classLoader != null : CLASS_LOADER_IS_NULL;
        URL url = classLoader.getResource(path);//"META-INF/xml/test.xml"
        Optional.ofNullable(url)
                .orElseThrow(() -> new IllegalArgumentTransactionExceptions(
                        messageService.getMessage(
                                NameErrorMessageEnumeration.PATH_NOT_CORRECT.getNameMessage())));
        return new FileInputStream(url.getFile());
    }

    public Transactions getAllTransactions() {

        try (FileInputStream inputStream =
                     getFileInputStream(messageService.getMessage(
                             FilePathEnumeration.PATH_TEST_XML.getPath()))){
            Optional.ofNullable(inputStream)
                    .orElseThrow(() -> new IllegalArgumentTransactionExceptions(
                            messageService.getMessage(
                                    NameErrorMessageEnumeration.PATH_NOT_CORRECT.getNameMessage())));
            return readLargeFileWithJaxb(inputStream);
        } catch (XMLStreamException
                | JAXBException
                | IOException ex) {
            ex.printStackTrace();
            throw new NotCorrectFileFormatException(messageService.getMessage(
                    NameErrorMessageEnumeration.NOT_CORRECT_FORMAT_FILE.getNameMessage()));
        }
    }


    public Transactions readLargeFileWithJaxb(FileInputStream fileInputStream)
            throws XMLStreamException, JAXBException {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader xmlStream = xmlInputFactory
                .createXMLStreamReader(fileInputStream);
        try {
            setStartElement(xmlStream, DocumentNameTagEnumeration.TRANSACTIONS.getNameTag());
            return unmarshallingTransactions(xmlStream);
        } finally {
            xmlStream.close();
        }

    }

    public Transactions unmarshallingTransactions(XMLStreamReader xmlStream)
            throws JAXBException, XMLStreamException {
        Optional.ofNullable(xmlStream).orElseThrow(() ->
                new IllegalArgumentTransactionExceptions(messageService.getMessage(
                        NameErrorMessageEnumeration.XML_STREAM_IS_NULL.getNameMessage())));
        JAXBElement<Transactions> transactionsJAXBElement = null;
        JAXBContext context = JAXBContext.newInstance(Transactions.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        while (xmlStream.getEventType() == XMLStreamConstants.START_ELEMENT) {
            transactionsJAXBElement = unmarshaller.unmarshal(xmlStream, Transactions.class);
            if (xmlStream.getEventType() == XMLStreamConstants.CHARACTERS) {
                xmlStream.next();
            }
        }
        Optional.ofNullable(transactionsJAXBElement).orElseThrow(() ->
                new NotCorrectFileFormatException(messageService.getMessage(
                        NameErrorMessageEnumeration.NOT_CORRECT_FORMAT_FILE.getNameMessage())));
        return transactionsJAXBElement.getValue();
    }

    public void setStartElement(XMLStreamReader xmlStream, String startElement) throws XMLStreamException {
        Optional.ofNullable(xmlStream).orElseThrow(() ->
                new IllegalArgumentTransactionExceptions(messageService.getMessage(
                        NameErrorMessageEnumeration.XML_STREAM_IS_NULL.getNameMessage())));
        boolean key = true;
        while (key) {
            xmlStream.nextTag();
            if (xmlStream.getName().toString().equals(startElement)) {
                key = false;
                xmlStream.require(XMLStreamConstants.START_ELEMENT,
                        null,
                        startElement);
            }
        }
        if (key) {
            throw new NotCorrectFileFormatException(messageService.getMessage(
                    NameErrorMessageEnumeration.NOT_CORRECT_FORMAT_FILE.getNameMessage()));
        }
    }


    public Long getSizeFileXml() {
        ClassLoader classLoader = getClass().getClassLoader();
        assert classLoader != null : CLASS_LOADER_IS_NULL;
        URL url = classLoader.getResource(messageService.getMessage(FilePathEnumeration.PATH_TEST_XML.getPath()));
        Optional.ofNullable(url)
                .orElseThrow(() -> new IllegalArgumentTransactionExceptions(
                        messageService.getMessage(
                                NameErrorMessageEnumeration.PATH_NOT_CORRECT.getNameMessage())));
        File file = new File(url.getFile());
        Optional.ofNullable(file)
                .orElseThrow(() -> new IllegalArgumentTransactionExceptions(
                        messageService.getMessage(
                                NameErrorMessageEnumeration.PATH_NOT_CORRECT.getNameMessage())));
        Long length = file.length();
        if (length >= 1024L * 1024L * 1024L) {
            throw new FileIsNullException(messageService.getMessage(
                    NameErrorMessageEnumeration.FILE_IS_NULL.getNameMessage()));
        }
        return length;
    }
}
