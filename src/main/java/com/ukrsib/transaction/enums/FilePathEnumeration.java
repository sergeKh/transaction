package com.ukrsib.transaction.enums;

public enum FilePathEnumeration {
    PATH_TEST_XML("path.test.xml.file");
    private String path;

    FilePathEnumeration(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
