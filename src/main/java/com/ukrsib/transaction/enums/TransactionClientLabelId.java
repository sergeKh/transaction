package com.ukrsib.transaction.enums;

public enum TransactionClientLabelId {
    INN("inn"),
    FIRST_NAME("firstName"),
    LAST_NAME("lastName"),
    MIDDLE_NAME("middleName");

    private String id;

    TransactionClientLabelId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
