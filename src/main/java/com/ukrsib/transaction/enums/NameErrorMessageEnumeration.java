package com.ukrsib.transaction.enums;

public enum NameErrorMessageEnumeration {
    PATH_NOT_CORRECT("exception.path.not.correct"),
    PATH_IS_NULL("exception.path.is.null"),
    DOCUMENT_BUILDER_IS_NULL("exception.cannot.create.builder"),
    MISSING_MESSAGE_TAG("exception.missing.message.tag"),
    NOT_CORRECT_FORMAT_FILE("exception.not.correct.format.file"),
    FILE_IS_EXTRA_LARGE("exception.file.is.large"),
    FILE_IS_NULL("exception.file.is.null"),
    XML_STREAM_IS_NULL("exception.xml.stream.is.null"),
    HASH_TRANSACTIONS_IS_NULL("exception.hash.transactions.null");

    NameErrorMessageEnumeration(String nameMessage) {
        this.nameMessage = nameMessage;
    }

    private String nameMessage;

    public String getNameMessage() {
        return nameMessage;
    }
}
