package com.ukrsib.transaction.enums;

public enum NameErrorFieldEnumerations {
    ERROR_FIELD_MESSAGES("errorMessage");

    private String nameField;

    NameErrorFieldEnumerations(String nameField) {
        this.nameField = nameField;
    }

    public String getNameField() {
        return nameField;
    }
}
