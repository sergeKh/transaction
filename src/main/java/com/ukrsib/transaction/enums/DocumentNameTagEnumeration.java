package com.ukrsib.transaction.enums;

public enum DocumentNameTagEnumeration {
    TRANSACTIONS("transactions");

    private String nameTag;

    DocumentNameTagEnumeration(String nameTag) {
        this.nameTag = nameTag;
    }

    public String getNameTag() {
        return nameTag;
    }

    public void setNameTag(String nameTag) {
        this.nameTag = nameTag;
    }
}
