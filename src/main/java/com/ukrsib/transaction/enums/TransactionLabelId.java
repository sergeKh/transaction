package com.ukrsib.transaction.enums;

public enum TransactionLabelId {
    PLACE("place"),
    CURRENCY("currency"),
    AMOUNT("amount"),
    CARD("card");

    private String id;

    TransactionLabelId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
